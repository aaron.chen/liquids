import i18next from 'i18next';

import { Languages } from '../Redux/Actions';

import enStrings from './Locales/en/strings.json';
import jaStrings from './Locales/ja/strings.json';

const { en } = Languages;

i18next.init({
  resources: {
    en: {
      translation: enStrings,
    },
    ja: {
      translation: jaStrings,
    },
  },
  fallbackLng: en,
  debug: true,
});

export default i18next;
