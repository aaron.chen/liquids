// White
export const WHITE = 'rgb(255,255,255)';

// Blacks
export const SLIGHT_TRANS_BLACK = 'rgba(0,0,0,0.8)';

// Greens
export const LIGHT_GREEN = 'rgb(150,255,185)';
export const DARK_GREEN = 'rgb(30,115,100)';
export const DARK_GREEN_DISABLED = 'rgba(30,115,100,0.5)';

// Greys
export const LIGHTEST_GREY = 'rgb(248,255,248)';
export const LIGHTEST_GREY_HOVER = 'rgb(110,195,170)';
export const LIGHT_GREY = 'rgb(235,240,235)';
export const GREY = 'rgb(132,132,132)';

// Reds
export const RED = 'rgb(255,50,0)';

// Shadows
export const SHADOW_FROM = 'rgb(0,0,0,0.16)';
export const SHADOW_TO = 'rgb(0,0,0,0.23)';

// Borders
export const GREY_HEADER_BORDER = 'rgba(0,0,0,0.1)';
