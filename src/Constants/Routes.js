export const DashboardRoute = '/';
export const AssetsRoute = '/Assets';
export const HistoryRoute = '/History';
export const ValuationRoute = '/Valuation';
