import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { SLIGHT_TRANS_BLACK } from '../../../Constants/Colors';

const StyledFunctionModalContent = styled.article`
  color: ${SLIGHT_TRANS_BLACK};
  padding: 5px;
`;

const FunctionModalContent = ({ className = '', children = null }) => {
  return <StyledFunctionModalContent className={className}>{children}</StyledFunctionModalContent>;
};

FunctionModalContent.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
};

export default FunctionModalContent;
