import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { DARK_GREEN, GREY_HEADER_BORDER, SLIGHT_TRANS_BLACK } from '../../../Constants/Colors';

const StyledFunctionModalHeader = styled.header`
  text-align: center;
  border-bottom: 1px solid ${GREY_HEADER_BORDER};
  padding-bottom: 10px;
  margin-bottom: 10px;
`;

const FaIcon = styled(FontAwesomeIcon)`
  font-size: 2.4em;
  vertical-align: middle;
  color: ${DARK_GREEN};
`;

const Title = styled.h1`
  font-size: 1.6em;
  font-weight: normal;
  color: ${SLIGHT_TRANS_BLACK};
  margin: 10px;
`;

const Subtitle = styled.p`
  font-size: 1.1em;
  color: ${SLIGHT_TRANS_BLACK};
`;

const FunctionModalHeader = ({ fa = '', title = '', subtitle = '' }) => {
  return (
    <StyledFunctionModalHeader>
      {fa && <FaIcon icon={fa} fixedWidth />}
      {title && <Title>{title}</Title>}
      {subtitle && <Subtitle>{subtitle}</Subtitle>}
    </StyledFunctionModalHeader>
  );
};

FunctionModalHeader.propTypes = {
  fa: PropTypes.string,
  title: PropTypes.string,
  subtitle: PropTypes.string,
};

export default FunctionModalHeader;
