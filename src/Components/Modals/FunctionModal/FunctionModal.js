import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { LIGHT_GREY } from '../../../Constants/Colors';

import Modal, { ModalContent } from '../Generic/Modal';
import FunctionModalHeader from './FunctionModalHeader';
import FunctionModalContent from './FunctionModalContent';
import FunctionModalFooter from './FunctionModalFooter';

const StyledFunctionModal = styled(Modal)`
  ${ModalContent} {
    display: flex;
    flex-direction: column;
    background-color: ${LIGHT_GREY};
    min-width: 550px;
  }
`;

const FunctionModal = ({
  className = '',
  children = null,

  hideHeader = false,
  hideFooter = false,

  fa = '',
  title = '',
  subtitle = '',

  buttons = [],

  hideCloseButton = false,
  onCloseClick = () => {},
}) => {
  return (
    <StyledFunctionModal
      className={className}
      hideCloseButton={hideCloseButton}
      onCloseButtonClick={onCloseClick}
      onOverlayClick={onCloseClick}
    >
      {!hideHeader && <FunctionModalHeader fa={fa} title={title} subtitle={subtitle} />}
      <FunctionModalContent>{children}</FunctionModalContent>
      {!hideFooter && <FunctionModalFooter buttons={buttons} />}
    </StyledFunctionModal>
  );
};

FunctionModal.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),

  hideHeader: PropTypes.bool,
  hideFooter: PropTypes.bool,

  fa: PropTypes.string,
  title: PropTypes.string,
  subtitle: PropTypes.string,

  buttons: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      label: PropTypes.string,
      type: PropTypes.string,
      disabled: PropTypes.bool,
      onClick: PropTypes.func,
    })
  ),

  hideCloseButton: PropTypes.bool,
  onCloseClick: PropTypes.func,
};

export default FunctionModal;
