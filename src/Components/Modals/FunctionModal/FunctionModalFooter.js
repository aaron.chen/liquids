import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import FlatButton from '../../Inputs/FlatButton';

const StyledFunctionModalFooter = styled.footer`
  display: flex;
  flex-direction: row-reverse;
  margin-top: 15px;
`;

// buttons shown in reverse order by default

const FunctionModalFooter = ({ className = '', buttons = [] }) => {
  const renderButtons = () => {
    return buttons.map(button => {
      const { id, label, type, primary, disabled, onClick } = button;

      return (
        <FlatButton key={id} type={type} primary={primary} disabled={disabled} onClick={onClick}>
          {label}
        </FlatButton>
      );
    });
  };

  return (
    <StyledFunctionModalFooter className={className}>{renderButtons()}</StyledFunctionModalFooter>
  );
};

FunctionModalFooter.propTypes = {
  className: PropTypes.string,
  buttons: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      label: PropTypes.string,
      type: PropTypes.string,
      primary: PropTypes.bool,
      disabled: PropTypes.bool,
      onClick: PropTypes.func,
    })
  ),
};

export default FunctionModalFooter;
