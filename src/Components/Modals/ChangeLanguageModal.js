import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';

import { changeLanguage, Languages } from '../../Redux/Actions';

import enUsLogo from '../../Images/Flags/united-states-of-america.svg';
import jaLogo from '../../Images/Flags/japan.svg';

import useI18n from '../../Hooks/useI18n';

import FunctionModal from './FunctionModal/FunctionModal';
import LanguageButton from '../Inputs/LanguageButton';

const LanguageButtonRow = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
`;

const ChangeLanguageModal = ({ className = '', onCloseClick = () => {} }) => {
  const t = useI18n();
  const dispatch = useDispatch();

  const renderLanguageButtons = () => {
    const { en, ja } = Languages;

    const lngButtons = [
      {
        id: 'usEnglish',
        src: enUsLogo,
        label: t('ChangeLanguageModal.usEnglish'),
        onClick: () => {
          dispatch(changeLanguage({ language: en }));
        },
      },
      {
        id: 'japanese',
        src: jaLogo,
        label: t('ChangeLanguageModal.japanese'),
        onClick: () => {
          dispatch(changeLanguage({ language: ja }));
        },
      },
    ];
    return (
      <LanguageButtonRow>
        {lngButtons.map(lngButton => {
          const { id, src, label, onClick } = lngButton;

          return (
            <LanguageButton
              key={id}
              src={src}
              label={label}
              onClick={() => {
                onClick();
                onCloseClick();
              }}
            />
          );
        })}
      </LanguageButtonRow>
    );
  };

  return (
    <FunctionModal
      className={className}
      hideFooter
      fa="language"
      title={t('ChangeLanguageModal.changeLanguage')}
      onCloseClick={onCloseClick}
    >
      {renderLanguageButtons()}
    </FunctionModal>
  );
};

ChangeLanguageModal.propTypes = {
  className: PropTypes.string,

  onCloseClick: PropTypes.func,
};

export default ChangeLanguageModal;
