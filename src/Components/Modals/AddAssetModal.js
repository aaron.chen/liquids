import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';

import { addAsset } from '../../Redux/Actions';

import useI18n from '../../Hooks/useI18n';

import FunctionModal from './FunctionModal/FunctionModal';
import LabeledInput from '../Inputs/LabeledInput';

const AddAssetModal = ({ className = '', onCloseClick = () => {} }) => {
  const t = useI18n();
  const dispatch = useDispatch();

  const [name, setName] = useState('');
  const [assetValue, setAssetValue] = useState(0);

  const nameValid = name.length > 0;
  const allValid = nameValid;

  const handleSubmit = () => {
    try {
      dispatch(addAsset({ name, assetValue }));
      onCloseClick();
    } catch (err) {
      console.error(err);
    }
  };

  const renderInputs = () => {
    const inputs = [
      {
        id: 'name',
        label: t('AddAssetModal.name'),
        value: name,
        placeholder: t('AddAssetModal.nameExample'),
        type: 'text',
        valid: nameValid,
        errorMessage: t('AddAssetModal.nameError'),
        onChange: e => setName(e.target.value),
      },
      {
        id: 'value',
        label: t('AddAssetModal.value'),
        value: assetValue,
        type: 'number',
        min: 0,
        step: 0.01,
        onChange: e => setAssetValue(Number(e.target.value)),
      },
    ];

    return (
      <>
        {inputs.map(input => {
          const {
            id,
            label,
            value,
            placeholder,
            type,
            min,
            step,
            valid,
            errorMessage,
            onChange,
          } = input;

          return (
            <LabeledInput
              key={id}
              label={label}
              value={value}
              placeholder={placeholder}
              type={type}
              min={min}
              step={step}
              valid={valid}
              errorMessage={errorMessage}
              onChange={onChange}
            />
          );
        })}
      </>
    );
  };

  const buttons = [
    {
      id: 'saveButton',
      label: t('save'),
      type: 'submit',
      onClick: handleSubmit,
      disabled: !allValid,
    },
    {
      id: 'cancelButton',
      label: t('cancel'),
      onClick: onCloseClick,
      primary: false,
    },
  ];

  return (
    <form>
      <FunctionModal
        className={className}
        fa="dollar-sign"
        title={t('AddAssetModal.addAsset')}
        buttons={buttons}
        onCloseClick={onCloseClick}
      >
        {renderInputs()}
      </FunctionModal>
    </form>
  );
};

AddAssetModal.propTypes = {
  className: PropTypes.string,

  onCloseClick: PropTypes.func,
};

export default AddAssetModal;
