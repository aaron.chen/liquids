import React, { useRef, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { DARK_GREEN, LIGHTEST_GREY, SHADOW_FROM, SHADOW_TO } from '../../../Constants/Colors';

import Overlay from './Overlay';

const CloseButtonIcon = styled(({ iconSize, ...otherProps }) => (
  <FontAwesomeIcon {...otherProps} />
))`
  position: absolute;
  top: ${props => -props.iconSize / 2}px;
  right: ${props => -props.iconSize / 2}px;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${LIGHTEST_GREY};
  color: ${DARK_GREEN};
  font-size: ${props => props.iconSize}px;
  box-shadow: ${SHADOW_FROM} 0px 3px 4px, ${SHADOW_TO} 0px 3px 4px;
  border-radius: 50%;
  cursor: pointer;
`;

export const ModalContent = styled.div`
  position: absolute;
  display: flex;
  top: 50%;
  left: 50%;
  min-height: 225px;
  min-width: 300px;
  max-height: 100%;
  box-sizing: border-box;
  padding: 20px;
  background-color: ${LIGHTEST_GREY};
  transition: all 300ms;
  transform-origin: 50% 50%;
  transform: ${props =>
    props.started ? 'translate(-50%, -50%)' : 'translate(-50%, -50%) scale(0.5, 0.5)'};
  visibility: ${props => (props.started ? '' : 'hidden')};
`;

const STARTED = Symbol('Started');
const UNSTARTED = Symbol('Unstarted');

const Modal = ({
  className = '',
  children = null,

  // modals can have a "X" button in the top-right corner
  hideCloseButton = false,
  onCloseButtonClick = () => {},

  overlayColor = 'rgba(0,0,0,0.5)',
  onOverlayClick = () => {},
}) => {
  const contentRef = useRef(null);
  const [animationState, setAnimationState] = useState(UNSTARTED);

  // start animating modal expansion
  useEffect(() => {
    setAnimationState(STARTED);
  }, []);

  const renderCloseButton = () => {
    if (hideCloseButton) {
      return null;
    }

    return (
      <CloseButtonIcon icon={['far', 'times-circle']} iconSize={24} onClick={onCloseButtonClick} />
    );
  };

  const renderContent = () => {
    return (
      <ModalContent ref={contentRef} started={animationState === STARTED}>
        {renderCloseButton()}
        {children}
      </ModalContent>
    );
  };

  return (
    <Overlay className={className} overlayColor={overlayColor} onClick={onOverlayClick}>
      {renderContent()}
    </Overlay>
  );
};

Modal.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),

  hideCloseButton: PropTypes.bool,
  onCloseButtonClick: PropTypes.func,

  overlayColor: PropTypes.string,
  onOverlayClick: PropTypes.func,
};

export default Modal;
