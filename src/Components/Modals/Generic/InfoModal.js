import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Modal, { ModalContent } from './Modal';
import FlatButton from '../../Inputs/FlatButton';

const StyledModal = styled(Modal)`
  ${ModalContent} {
    display: flex;
    flex-direction: column;
    width: 350px;
    padding: 20px;
    text-overflow: ellipsis;
    z-index: 1001;
  }
`;

const Title = styled.h1`
  font-size: 0.8em;
  font-weight: bold;
  line-height: 20px;
  margin-bottom: 20px;
`;

const Description = styled.p`
  font-size: 0.65em;
  line-height: 0.8125em;
  margin-bottom: 30px;
`;

const ButtonRow = styled.div`
  display: flex;
  flex-direction: row-reverse;
`;

const StyledFlatButton = styled(FlatButton)`
  width: 6em;
  font-size: 0.6em;
`;

const InfoModal = ({
  className = '',

  title = '',
  description = '',

  confirmText = 'OK',
  cancelText = 'Cancel',

  hideConfirm = false,
  hideCancel = false,

  onConfirm = () => {},
  onCancel = () => {},
}) => {
  const renderControls = () => {
    return (
      <ButtonRow>
        {!hideConfirm && <StyledFlatButton onClick={onConfirm}>{confirmText}</StyledFlatButton>}
        {!hideCancel && (
          <StyledFlatButton primary={false} onClick={onCancel}>
            {cancelText}
          </StyledFlatButton>
        )}
      </ButtonRow>
    );
  };

  const spacer = <div style={{ flexGrow: 1 }} />;

  return (
    <StyledModal className={className}>
      <Title>{title}</Title>
      <Description>{description}</Description>
      {spacer}
      {renderControls()}
    </StyledModal>
  );
};

InfoModal.propTypes = {
  className: PropTypes.string,

  title: PropTypes.string,
  description: PropTypes.string,

  confirmText: PropTypes.string,
  cancelText: PropTypes.string,
  hideConfirm: PropTypes.bool,
  hideCancel: PropTypes.bool,

  onConfirm: PropTypes.func,
  onCancel: PropTypes.func,
};

export default InfoModal;
