import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledOverlay = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 1000;
  background-color: ${props => (props.started ? props.overlayColor : 'rgba(0,0,0,0)')};
  transition-property: opacity, background;
  transition-duration: 300ms;
  transition-timing-function: cubic-bezier(0.23, 1, 0.32, 1);
  transition-deplay: 0ms;
`;

const Overlay = ({
  className = '',
  children = null,
  overlayColor = 'rgba(0,0,0,0.5)',
  onClick = () => {},
}) => {
  const [started, setStarted] = useState(false);

  // begin animating on mount
  useEffect(() => {
    setStarted(true);
  }, []);

  // clicking on children (i.e. content - like a modal)
  // should not trigger onClick
  const handleChildrenClick = useCallback(event => {
    event.stopPropagation();
  }, []);

  return (
    <StyledOverlay
      className={className}
      started={started}
      overlayColor={overlayColor}
      onClick={onClick}
    >
      <div onClick={handleChildrenClick} role="presentation">
        {children}
      </div>
    </StyledOverlay>
  );
};

Overlay.propTypes = {
  className: PropTypes.string,
  overlayColor: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  onClick: PropTypes.func,
};

export default Overlay;
