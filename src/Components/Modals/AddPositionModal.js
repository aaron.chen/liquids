import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';

import { PositionTypes, addPosition } from '../../Redux/Actions';

import useI18n from '../../Hooks/useI18n';

import FunctionModal from './FunctionModal/FunctionModal';
import LabeledInput from '../Inputs/LabeledInput';
import LabeledSelect from '../Inputs/LabeledSelect';

const { EQUITY, FUND } = PositionTypes;

const AddPositionModal = ({ className = '', onCloseClick = () => {} }) => {
  const t = useI18n();
  const dispatch = useDispatch();

  const [assetType, setAssetType] = useState(EQUITY);
  const [symbol, setSymbol] = useState('');
  const [name, setName] = useState('');

  const symbolValid = symbol.length > 0;
  const allValid = symbolValid;

  const handleSubmit = () => {
    try {
      dispatch(addPosition({ assetType, symbol, name }));
      onCloseClick();
    } catch (err) {
      console.error(err);
    }
  };

  const renderSelects = () => {
    return (
      <LabeledSelect
        label={t('AddPositionModal.assetType')}
        value={assetType}
        onChange={e => setAssetType(e.target.value)}
      >
        <option value={EQUITY}>{t('equity')}</option>
        <option value={FUND}>{t('fund')}</option>
      </LabeledSelect>
    );
  };

  const renderInputs = () => {
    const inputs = [
      {
        id: 'symbol',
        label: t('AddPositionModal.symbol'),
        value: symbol,
        placeholder: t('AddPositionModal.symbolExample'),
        type: 'text',
        valid: symbolValid,
        errorMessage: t('AddPositionModal.symbolError'),
        onChange: e => {
          setSymbol(e.target.value);
        },
      },
      {
        id: 'name',
        label: t('AddPositionModal.name'),
        value: name,
        placeholder: t('AddPositionModal.nameExample'),
        type: 'text',
        onChange: e => setName(e.target.value),
      },
    ];

    return (
      <>
        {inputs.map(input => {
          const {
            id,
            label,
            value,
            placeholder,
            type,
            min,
            step,
            valid,
            errorMessage,
            onChange,
          } = input;

          return (
            <LabeledInput
              key={id}
              label={label}
              value={value}
              placeholder={placeholder}
              type={type}
              min={min}
              step={step}
              valid={valid}
              errorMessage={errorMessage}
              onChange={onChange}
            />
          );
        })}
      </>
    );
  };

  const buttons = [
    {
      id: 'saveButton',
      label: t('save'),
      type: 'submit',
      onClick: handleSubmit,
      disabled: !allValid,
    },
    {
      id: 'cancelButton',
      label: t('cancel'),
      onClick: onCloseClick,
      primary: false,
    },
  ];

  return (
    <form>
      <FunctionModal
        className={className}
        fa="coins"
        title={t('AddPositionModal.addPosition')}
        buttons={buttons}
        onCloseClick={onCloseClick}
      >
        {renderSelects()}
        {renderInputs()}
      </FunctionModal>
    </form>
  );
};

AddPositionModal.propTypes = {
  className: PropTypes.string,

  onCloseClick: PropTypes.func,
};

export default AddPositionModal;
