import React, { useRef, useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { DARK_GREEN } from '../../Constants/Colors';
import { DashboardRoute, AssetsRoute, HistoryRoute, ValuationRoute } from '../../Constants/Routes';

import { hydrateStore } from '../../Redux/Actions';

import useI18n from '../../Hooks/useI18n';

import SidebarItem from './SidebarItem';
import ChangeLanguageModal from '../Modals/ChangeLanguageModal';

const CHANGE_LANGUAGE_MODAL = Symbol('Show Change Language Modal');

const StyledSidebar = styled.nav`
  display: flex;
  flex-direction: column;
  background-color: ${DARK_GREEN};
  min-width: 230px;
  min-height: 100vh;
`;

const ImportDataInput = styled.input`
  display: none;
`;

const ExportDataAnchor = styled.a`
  display: none;
`;

const Sidebar = ({ className = '' }) => {
  const t = useI18n();
  const store = useSelector(state => state);
  const dispatch = useDispatch();

  const importDataInputRef = useRef();
  const exportDataAnchorRef = useRef();

  const [functionModal, setFunctionModal] = useState(null);

  const renderFunctionModal = () => {
    switch (functionModal) {
      case CHANGE_LANGUAGE_MODAL:
        return <ChangeLanguageModal onCloseClick={() => setFunctionModal(null)} />;
      default:
        console.error('Invalid function modal');
        return null;
    }
  };

  const renderLinks = () => {
    const links = [
      {
        id: 'dashboard',
        fa: 'th',
        name: t('Sidebar.dashboard'),
        route: DashboardRoute,
      },
      {
        id: 'assets',
        fa: 'dollar-sign',
        name: t('Sidebar.assets'),
        route: AssetsRoute,
      },
      {
        id: 'history',
        fa: 'history',
        name: t('Sidebar.history'),
        route: HistoryRoute,
      },
      {
        id: 'valuation',
        fa: 'tree',
        name: t('Sidebar.valuation'),
        route: ValuationRoute,
      },
      {
        id: 'importData',
        fa: 'file-import',
        name: t('Sidebar.importData'),
        onClick: () => {
          importDataInputRef.current.click();
        },
      },
      {
        id: 'exportData',
        fa: 'file-export',
        name: t('Sidebar.exportData'),
        onClick: () => {
          exportDataAnchorRef.current.click();
        },
      },
      {
        id: 'language',
        fa: 'language',
        name: t('Sidebar.language'),
        onClick: () => setFunctionModal(CHANGE_LANGUAGE_MODAL),
      },
    ];

    return links.map(link => {
      const { id, fa, name, route, onClick } = link;
      return <SidebarItem key={id} fa={fa} label={name} route={route} onClick={onClick} />;
    });
  };

  const renderImportInput = () => {
    return (
      <ImportDataInput
        ref={importDataInputRef}
        type="file"
        accept="*.json"
        name="importFile"
        onChange={event => {
          const { files } = event.target;
          const file = files[0];

          const fileReader = new FileReader();
          fileReader.onloadend = () => {
            try {
              const importedJSON = JSON.parse(fileReader.result);

              const { language, options } = importedJSON;

              const newStore = {
                language,
                options,
              };

              dispatch(hydrateStore({ newStore }));
            } catch (err) {
              console.error(err);
              console.error('could not hydrate store');
              dispatch(hydrateStore({ newStore: store }));
            }
          };

          try {
            fileReader.readAsBinaryString(file);
          } catch (err) {
            console.error(err);
            console.error('could not read file');
          }
        }}
      />
    );
  };

  const renderExportAnchor = () => {
    const exportJSON = encodeURIComponent(JSON.stringify(store));
    const exportString = `data:text/json;charset=utf-8,${exportJSON}`;

    const fileName = `Export-${new Date().toISOString()}.json`;

    return <ExportDataAnchor ref={exportDataAnchorRef} href={exportString} download={fileName} />;
  };

  return (
    <StyledSidebar className={className}>
      {functionModal && renderFunctionModal()}
      {renderLinks()}
      {renderImportInput()}
      {renderExportAnchor()}
    </StyledSidebar>
  );
};

Sidebar.propTypes = {
  className: PropTypes.string,
};

export default Sidebar;
