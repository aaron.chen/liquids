import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { A } from 'hookrouter';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { LIGHTEST_GREY_HOVER, LIGHTEST_GREY } from '../../Constants/Colors';

const StyledSidebarItem = styled(A)`
  text-decoration: none;
`;

const SidebarItemRow = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  height: 3em;
  padding: 0 1em;
  font-size: 1.2em;
  cursor: pointer;
  color: ${LIGHTEST_GREY};

  :hover {
    color: ${LIGHTEST_GREY_HOVER};
  }
`;

const SidebarItemIcon = styled(FontAwesomeIcon)`
  transition: color 300ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;
`;

const SidebarItemText = styled.div`
  padding-left: 1em;
  transition: color 300ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;
`;

const SidebarItem = ({ className = '', fa = '', label = '', route = '', onClick = () => {} }) => {
  const baseRef = useRef();

  return (
    <StyledSidebarItem className={className} href={route}>
      <SidebarItemRow ref={baseRef} onClick={onClick}>
        <SidebarItemIcon icon={fa} fixedWidth />
        <SidebarItemText>{label}</SidebarItemText>
      </SidebarItemRow>
    </StyledSidebarItem>
  );
};

SidebarItem.propTypes = {
  className: PropTypes.string,
  fa: PropTypes.string,
  label: PropTypes.string,
  route: PropTypes.string,
  onClick: PropTypes.func,
};

export default SidebarItem;
