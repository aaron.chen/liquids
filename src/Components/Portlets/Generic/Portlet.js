import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { WHITE, LIGHTEST_GREY, DARK_GREEN, GREY_HEADER_BORDER } from '../../../Constants/Colors';

const StyledPortlet = styled.section`
  border: 1px solid ${GREY_HEADER_BORDER};
  border-radius: 10px;
  background-color: ${WHITE};
`;

const PortletHeader = styled.header`
  font-size: 1.3em;
  color: ${LIGHTEST_GREY};
  background-color: ${DARK_GREEN};
  border-radius: 10px 10px 0 0;
  padding: 3px 7.5px;
`;

const PortletContent = styled.article`
  padding: 10px;
`;

const Portlet = ({ className = '', children = null, title = '', hideHeader = false }) => {
  return (
    <StyledPortlet className={className}>
      {!hideHeader && <PortletHeader>{title}</PortletHeader>}
      <PortletContent>{children}</PortletContent>
    </StyledPortlet>
  );
};

Portlet.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),

  title: PropTypes.string,
  hideHeader: PropTypes.bool,
};

export default Portlet;
