import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

// import { DARK_GREEN, GREY_HEADER_BORDER } from '../../Constants/Colors';

import Portlet from './Generic/Portlet';

const AssetsPortlet = ({ className = '', hideHeader = false }) => {
  return (
    <Portlet className={className} title="Assets" hideHeader={hideHeader}>
      Insert Asset List Here
    </Portlet>
  );
};

AssetsPortlet.propTypes = {
  className: PropTypes.string,

  hideHeader: PropTypes.bool,
};

export default AssetsPortlet;
