import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Sidebar from './Sidebar/Sidebar';
import Content from './Content/Content';

const StyledApp = styled.div`
  display: flex;
  flex-direction: row;
  font-family: 'Noto Sans', -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
    'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
`;

const App = ({ className = '' }) => {
  return (
    <StyledApp className={className}>
      <Sidebar />
      <Content />
    </StyledApp>
  );
};

App.propTypes = {
  className: PropTypes.string,
};

export default App;
