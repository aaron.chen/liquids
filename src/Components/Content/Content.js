import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useRoutes } from 'hookrouter';

import { LIGHTEST_GREY, DARK_GREEN } from '../../Constants/Colors';
import { DashboardRoute, AssetsRoute, HistoryRoute, ValuationRoute } from '../../Constants/Routes';

import DashboardPage from './Pages/DashboardPage';
import AssetsPage from './Pages/AssetsPage';
import HistoryPage from './Pages/HistoryPage';
import ValuationPage from './Pages/ValuationPage';

const StyledContent = styled.div`
  background-color: ${LIGHTEST_GREY};
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  color: ${DARK_GREEN};
`;

const routes = {
  [DashboardRoute]: () => <DashboardPage />,
  [AssetsRoute]: () => <AssetsPage />,
  [HistoryRoute]: () => <HistoryPage />,
  [ValuationRoute]: () => <ValuationPage />,
};

const Content = ({ className = '' }) => {
  const routeResult = useRoutes(routes);

  return <StyledContent className={className}>{routeResult}</StyledContent>;
};

Content.propTypes = {
  className: PropTypes.string,
};

export default Content;
