import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import useI18n from '../../../Hooks/useI18n';

import PageHeader from './Generic/PageHeader';
import ActionButtonRow from './Generic/ActionButtonRow';
import AddPositionModal from '../../Modals/AddPositionModal';
import AddAssetModal from '../../Modals/AddAssetModal';
import AssetsPortlet from '../../Portlets/AssetsPortlet';

const StyledAssetsPage = styled.div`
  display: flex;
  flex-direction: column;
  padding: 20px;
`;

const Ul = styled.ul`
  color: black;
`;

const Li = styled.li`
  color: black;
`;

const LiLabel = styled.div`
  color: black;
  font-weight: bold;
`;

const LiValue = styled.div`
  color: black;
`;

const ADD_POSITION_MODAL = Symbol('Show Add Position Modal');
const ADD_ASSET_MODAL = Symbol('Show Add Asset Modal');

const AssetsPage = ({ className = '' }) => {
  const t = useI18n();
  const positions = useSelector(state => state.positions);

  const [functionModal, setFunctionModal] = useState(null);

  const renderFunctionModal = () => {
    switch (functionModal) {
      case ADD_POSITION_MODAL:
        return <AddPositionModal onCloseClick={() => setFunctionModal(null)} />;
      case ADD_ASSET_MODAL:
        return <AddAssetModal onCloseClick={() => setFunctionModal(null)} />;
      default:
        console.error('Invalid Function Modal');
        return null;
    }
  };

  const renderActions = () => {
    const actionButtons = [
      {
        id: 'addAsset',
        label: t('AssetsPage.addAsset'),
        onClick: () => setFunctionModal(ADD_ASSET_MODAL),
      },
      {
        id: 'addPosition',
        label: t('AssetsPage.addPosition'),
        onClick: () => setFunctionModal(ADD_POSITION_MODAL),
      },
    ];

    return <ActionButtonRow buttons={actionButtons} />;
  };

  const renderAssets = () => {
    return <AssetsPortlet />;
  };

  const renderPositions = () => {
    return (
      <Ul>
        {Object.keys(positions).map(symbol => {
          const position = positions[symbol];
          const { name, assetType, initialQuantity, initialCost } = position;

          return (
            <Li key={symbol}>
              <LiLabel>Symbol:&nbsp;</LiLabel>
              <LiValue>{symbol}</LiValue>
              <LiLabel>Name:&nbsp;</LiLabel>
              <LiValue>{name}</LiValue>
              <LiLabel>Asset Type:&nbsp;</LiLabel>
              <LiValue>{assetType}</LiValue>
              <LiLabel>Initial Quantity:&nbsp;</LiLabel>
              <LiValue>{initialQuantity}</LiValue>
              <LiLabel>Initial Cost:&nbsp;</LiLabel>
              <LiValue>{initialCost}</LiValue>
            </Li>
          );
        })}
      </Ul>
    );
  };

  return (
    <StyledAssetsPage className={className}>
      <PageHeader title={t('AssetsPage.assets')} />
      {functionModal && renderFunctionModal()}
      {renderActions()}
      {renderAssets()}
      {renderPositions()}
    </StyledAssetsPage>
  );
};

AssetsPage.propTypes = {
  className: PropTypes.string,
};

export default AssetsPage;
