import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import useI18n from '../../../Hooks/useI18n';

import PageHeader from './Generic/PageHeader';

const StyledDashboardPage = styled.div`
  display: flex;
  flex-direction: column;
`;

const DashboardPage = ({ className = '' }) => {
  const t = useI18n();

  return (
    <StyledDashboardPage className={className}>
      <PageHeader title={t('DashboardPage.dashboard')} />
    </StyledDashboardPage>
  );
};

DashboardPage.propTypes = {
  className: PropTypes.string,
};

export default DashboardPage;
