import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledPageHeader = styled.h1`
  font-family: 'Noto Sans';
  font-size: 2.15em;
  font-weight: normal;
  margin: 0 0 0.5em;
`;

const PageHeader = ({ className = '', title = '' }) => {
  return <StyledPageHeader className={className}>{title}</StyledPageHeader>;
};

PageHeader.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
};

export default PageHeader;
