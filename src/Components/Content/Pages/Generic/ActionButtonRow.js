import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import FlatButton from '../../../Inputs/FlatButton';

const StyledActionButtonRow = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: 0.5em;
`;

const StyledFlatButton = styled(FlatButton)`
  width: 8.2em;
  :first-of-type {
    margin-left: 0;
  }
`;

const ActionButtonRow = ({ buttons = [] }) => {
  return (
    <StyledActionButtonRow>
      {buttons.map(button => {
        const { id, label, onClick } = button;

        return (
          <StyledFlatButton key={id} onClick={onClick}>
            {label}
          </StyledFlatButton>
        );
      })}
    </StyledActionButtonRow>
  );
};

ActionButtonRow.propTypes = {
  buttons: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      label: PropTypes.string,
      onClick: PropTypes.func,
    })
  ),
};

export default ActionButtonRow;
