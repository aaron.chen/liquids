import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import useI18n from '../../../Hooks/useI18n';

import PageHeader from './Generic/PageHeader';

const StyledValuationPage = styled.div`
  display: flex;
  flex-direction: column;
`;

const ValuationPage = ({ className = '' }) => {
  const t = useI18n();

  return (
    <StyledValuationPage className={className}>
      <PageHeader title={t('ValuationPage.valuation')} />
    </StyledValuationPage>
  );
};

ValuationPage.propTypes = {
  className: PropTypes.string,
};

export default ValuationPage;
