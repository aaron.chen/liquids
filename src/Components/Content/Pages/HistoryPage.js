import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import useI18n from '../../../Hooks/useI18n';

import PageHeader from './Generic/PageHeader';

const StyledHistoryPage = styled.div`
  display: flex;
  flex-direction: column;
`;

const HistoryPage = ({ className = '' }) => {
  const t = useI18n();

  return (
    <StyledHistoryPage className={className}>
      <PageHeader title={t('HistoryPage.history')} />
    </StyledHistoryPage>
  );
};

HistoryPage.propTypes = {
  className: PropTypes.string,
};

export default HistoryPage;
