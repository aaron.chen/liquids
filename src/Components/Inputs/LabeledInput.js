import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { GREY, GREY_HEADER_BORDER, SLIGHT_TRANS_BLACK } from '../../Constants/Colors';

import TopLabel from './TopLabel';
import BottomError from './BottomError';

const StyledLabelInput = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 0.4em;
`;

const InputWrapper = styled.div`
  display: flex;
  flex-direction: row;
`;

const Input = styled.input`
  color: ${GREY};
  padding: 0.35em 0.55em;
  font-size: 1.05em;
  border-radius: 6px;
  border: 1px solid ${GREY_HEADER_BORDER};
  flex-grow: 1;

  :focus {
    border: 1px solid ${SLIGHT_TRANS_BLACK}
  }
`;

const LabeledInput = ({
  className = '',
  label = '',
  valid = true,
  errorMessage = '',
  onChange = () => {},

  checked = false,
  disabled = false,
  max = '',
  min = '',
  name = '',
  placeholder = '',
  readonly = false,
  step = 1,
  type = 'text',
  value = '',
}) => {
  const [showErrorMessage, setShowErrorMessage] = useState(false);

  const handleChange = event => {
    setShowErrorMessage(false);
    onChange(event);
  };

  const handleBlur = () => {
    setShowErrorMessage(true);
  };

  const renderInput = () => {
    return (
      <InputWrapper>
        <Input
          onBlur={handleBlur}
          onChange={handleChange}
          checked={checked}
          disabled={disabled}
          max={max}
          min={min}
          name={name}
          placeholder={placeholder}
          readonly={readonly}
          step={step}
          type={type}
          value={value}
        />
      </InputWrapper>
    );
  };

  return (
    <StyledLabelInput className={className}>
      <TopLabel>{label}</TopLabel>
      {renderInput()}
      {showErrorMessage && !valid && <BottomError>{errorMessage}</BottomError>}
    </StyledLabelInput>
  );
};

LabeledInput.propTypes = {
  className: PropTypes.string,

  label: PropTypes.string,
  valid: PropTypes.bool,
  errorMessage: PropTypes.string,
  onChange: PropTypes.func,

  checked: PropTypes.bool,
  disabled: PropTypes.bool,
  max: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  min: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  name: PropTypes.string,
  placeholder: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  readonly: PropTypes.bool,
  step: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  type: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

export default LabeledInput;
