import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { RED } from '../../Constants/Colors';

const StyledBottomError = styled.div`
  color: ${RED};
  font-size: 0.8em;
`;

const BottomError = ({ className = '', children = null }) => {
  return <StyledBottomError className={className}>{children}</StyledBottomError>;
};

BottomError.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
};

export default BottomError;
