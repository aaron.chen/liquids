import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { GREY, GREY_HEADER_BORDER } from '../../Constants/Colors';

import TopLabel from './TopLabel';

const StyledLabelInput = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 0.4em;
`;

const SelectWrapper = styled.div`
  display: flex;
  flex-direction: row;
`;

const Select = styled.select`
  color: ${GREY};
  padding: 0.35em 0.55em;
  font-size: 1.05em;
  border-radius: 6px;
  border: 1px solid ${GREY_HEADER_BORDER};
  flex-grow: 1;
`;

const LabeledSelect = ({
  className = '',
  children = null,

  label = '',
  onChange = () => {},

  disabled = false,
  name = '',
  value = '',
}) => {
  const renderSelect = () => {
    return (
      <SelectWrapper>
        <Select onChange={onChange} disabled={disabled} name={name} value={value}>
          {children}
        </Select>
      </SelectWrapper>
    );
  };

  return (
    <StyledLabelInput className={className}>
      <TopLabel>{label}</TopLabel>
      {renderSelect()}
    </StyledLabelInput>
  );
};

LabeledSelect.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),

  label: PropTypes.string,
  onChange: PropTypes.func,

  disabled: PropTypes.bool,
  name: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
};

export default LabeledSelect;
