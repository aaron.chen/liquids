import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { SHADOW_FROM, SHADOW_TO } from '../../Constants/Colors';

const StyledLanguageButton = styled.div`
  display: flex;
  flex-direction: column;
  cursor: pointer;
  margin: 7.5px;
`;

const Logo = styled.img`
  border-radius: 50%;
  box-shadow: ${SHADOW_FROM} 0px 0px 10px, ${SHADOW_TO} 0px 0px 10px;
  width: 200px;
`;

const Label = styled.div`
  font-size: 1.2em;
  text-align: center;
  margin-top: 15px;
`;

const LanguageButton = ({ className = '', src = '', label = '', onClick = () => {} }) => {
  return (
    <StyledLanguageButton className={className} onClick={onClick}>
      <Logo src={src} alt={label} />
      <Label>{label}</Label>
    </StyledLanguageButton>
  );
};

LanguageButton.propTypes = {
  className: PropTypes.string,
  src: PropTypes.string,
  label: PropTypes.string,

  onClick: PropTypes.func,
};

export default LanguageButton;
