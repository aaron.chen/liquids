import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { GREY } from '../../Constants/Colors';

const StyledTopLabel = styled.div`
  color: ${GREY};
  font-size: 0.9em;
  font-weight: bold;
  margin-bottom: 0.25em;
`;

const TopLabel = ({ className = '', children = null }) => {
  return <StyledTopLabel className={className}>{children}</StyledTopLabel>;
};

TopLabel.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
};

export default TopLabel;
