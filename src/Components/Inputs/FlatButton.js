import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { DARK_GREEN, DARK_GREEN_DISABLED, LIGHTEST_GREY } from '../../Constants/Colors';

const StyledFlatButton = styled.button`
  margin: 0.15em 0.5em;
  padding: 0.25em 0.8em;
  min-width: 6em;
  font-size: 1.1em;
  background-color: ${props => (props.primary ? DARK_GREEN : LIGHTEST_GREY)};
  color: ${props => (props.primary ? LIGHTEST_GREY : DARK_GREEN)};
  border: 0.01em solid ${DARK_GREEN};
  border-radius: 3px;
  cursor: pointer;

  :disabled {
    background-color: ${props => (props.primary ? DARK_GREEN_DISABLED : LIGHTEST_GREY)};
    color: ${props => (props.primary ? LIGHTEST_GREY : DARK_GREEN_DISABLED)};
    border: 0.01em solid ${DARK_GREEN_DISABLED};
    cursor: not-allowed;
`;

const FlatButton = ({
  className = '',
  children = null,
  primary = true,
  disabled = false,
  type = 'button',
  onClick = () => {},
}) => {
  return (
    <StyledFlatButton
      className={className}
      primary={primary}
      disabled={disabled}
      type={type}
      onClick={onClick}
    >
      {children}
    </StyledFlatButton>
  );
};

FlatButton.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  primary: PropTypes.bool,
  disabled: PropTypes.bool,
  type: PropTypes.string,
  onClick: PropTypes.func,
};

export default FlatButton;
