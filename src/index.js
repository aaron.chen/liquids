import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import 'typeface-noto-sans';

import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faCoins,
  faDollarSign,
  faFileExport,
  faFileImport,
  faFileMedical,
  faHistory,
  faLanguage,
  faPlus,
  faTh,
  faTree,
} from '@fortawesome/free-solid-svg-icons';

import { faTimesCircle } from '@fortawesome/free-regular-svg-icons';

import './Styles/index.css';

import * as serviceWorker from './serviceWorker';

import './i18n/i18n';
import rootReducer from './Redux/Reducers';
import App from './Components/App';

// initialize font awesome library
library.add(
  faCoins,
  faDollarSign,
  faFileExport,
  faFileImport,
  faFileMedical,
  faHistory,
  faLanguage,
  faPlus,
  faTh,
  faTimesCircle,
  faTree
);

// create store
const store = createStore(rootReducer);

// render App
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
