/*
useHover.js

Hook that given a ref to a DOM node, returns true if
user is has their mouse over that node, and false otherwise.
*/

import { useState, useEffect } from 'react';

// see https://usehooks.com/ (MIT license)
const useHover = ref => {
  const [isHover, setIsHover] = useState(false);

  const handleMouseOver = () => setIsHover(true);
  const handleMouseOut = () => setIsHover(false);

  useEffect(
    () => {
      if (ref && ref.current) {
        const node = ref.current;
        node.addEventListener('mouseover', handleMouseOver);
        node.addEventListener('mouseout', handleMouseOut);

        return () => {
          node.removeEventListener('mouseover', handleMouseOver);
          node.removeEventListener('mouseout', handleMouseOut);
        };
      }
      return () => {};
    },
    [ref] // Recall only if ref changes
  );

  return isHover;
};

export default useHover;
