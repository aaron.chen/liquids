import { useSelector } from 'react-redux';

import i18n from '../i18n/i18n';
import { Languages } from '../Redux/Actions';

const translationFunctions = Object.keys(Languages).reduce((accFunctionsObj, currLngKey) => {
  const lngVal = Languages[currLngKey];
  return { ...accFunctionsObj, [currLngKey]: i18n.getFixedT(lngVal) };
}, {});

const useI18n = () => {
  const language = useSelector(state => state.language);

  return translationFunctions[language];
};

export default useI18n;
