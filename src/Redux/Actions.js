// ACTION TYPES
export const HYDRATE_STORE = 'HYDRATE_STORE';

export const CHANGE_LANGUAGE = 'SET_LANGUAGE';

export const ADD_POSITION = 'ADD_OPTION';
export const ADD_ASSET = 'ADD_ASSET';

// ACTION PARAMETERS
export const Languages = {
  en: 'en',
  ja: 'ja',
};

export const PositionTypes = {
  FUND: 'FUND',
  EQUITY: 'EQUITY',
};

// ACTION CREATORS
export const hydrateStore = ({ newStore }) => {
  return { type: HYDRATE_STORE, newStore };
};

export const changeLanguage = ({ language }) => {
  return { type: CHANGE_LANGUAGE, language };
};

export const addAsset = ({ name, assetValue }) => {
  return { type: ADD_ASSET, name, assetValue };
};

export const addPosition = ({ assetType, symbol, name }) => {
  return { type: ADD_POSITION, assetType, symbol, name };
};
