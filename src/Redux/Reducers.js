import { HYDRATE_STORE, CHANGE_LANGUAGE, ADD_POSITION, ADD_ASSET, Languages } from './Actions';

const { en } = Languages;

const initialState = {
  language: en,
  positions: {},
  otherAssets: {},
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case HYDRATE_STORE: {
      const { newStore } = action;

      return {
        ...state,
        ...newStore,
      };
    }
    case CHANGE_LANGUAGE: {
      const { language } = action;

      return {
        ...state,
        language,
      };
    }
    case ADD_POSITION: {
      const { symbol, name, assetType } = action;

      return {
        ...state,
        positions: {
          ...state.positions,
          [symbol]: {
            assetType,
            name,
          },
        },
      };
    }
    case ADD_ASSET: {
      const { id, name, assetValue } = action;

      return {
        ...state,
        otherAssets: {
          [id]: {
            name,
            assetValue,
          },
        },
      };
    }
    default:
      return state;
  }
};

export default rootReducer;
