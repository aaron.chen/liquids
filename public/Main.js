// eslint-disable-next-line import/no-extraneous-dependencies
const { app, BrowserWindow } = require('electron');
const path = require('path');

const createWindow = () => {
  // Create the browser window.
  const browserWindow = new BrowserWindow({ width: 1000, height: 750 });

  // and load the index.html of the app,
  const filePath = path.join(__dirname, '../build/index.html');
  browserWindow.loadURL(`file://${filePath}`);

  // Dev Tools
  browserWindow.webContents.openDevTools();
};

app.on('ready', createWindow);
